#include <string>
#include <iostream>
#include <fstream>
#include <math.h>
#include "Device.hpp"
using namespace std;
int main(int argc, char** argv)
{
  if(argc > 1)
  {
    int k = std::atoi(argv[1]);
    if(k%2 ==0)
  {
    string fileName = "fatTree-"+to_string(k)+".topo";
    fstream fichier;
    fichier.open(fileName,std::ios_base::out);
    if(!fichier.is_open())
    {
      printf("failed to open\n");
    }
    int nbNodesCoreLevel = (k/2)*(k/2);
    int nbNodesByPod = (k/2)*(k/2);
    int nbNodesEdgeLevel = (k/2) * k;
    int nbLeafNodes = nbNodesByPod * k;
    int nbNodesAggregationLevel = (k/2)*k;
    fichier<<"#nombre pods : "<< k<<"\n";
    fichier<<"#nombre noeuds feuilles : "<< nbLeafNodes <<"\n";
    std::vector<Device*> listLeafNodes;
    std::vector<Device*> listEdgeNodes;
    std::vector<Device*> listAggrNodes;
    std::vector<Device*> listCoreNodes;
    fichier<<"\n\n #Leaf Level en dessous \n\n";
    for(int iLoop = 0; iLoop< nbLeafNodes; iLoop++)
    {
      Device * d  = new Device(Device::Leaf);
      d->setIdDevice(iLoop);
      fichier<<*d;
      int numLinkedPod = iLoop/pow(k/2,2);
      int numLinkedSwitch = (iLoop % nbNodesByPod)/(k/2);
      fichier<<"[1] \t\"Edge(" <<numLinkedPod << ","<< numLinkedSwitch<<","<<1<<")\"" << "["<< (iLoop % (k/2)+1)*2 <<"]"<<endl;
      listLeafNodes.push_back(d);
    }
    fichier<<"\n\n #Edge Level en dessous \n\n";
    for(int edgeLoop = 0; edgeLoop < nbNodesEdgeLevel; edgeLoop++)
    {
      Device * edgeNode = new Device(Device::levelDev::Edge);

      edgeNode->setIdDevice({edgeLoop/(k/2),edgeLoop%(k/2),1});
      int numSwitch = edgeLoop%(k/2);
      int numPod = edgeLoop/(k/2);
      edgeNode->setPortNumberDevice(k);
      fichier<<endl<<*edgeNode;
      for(int connectionEdgeLevelLoop = 1 ; connectionEdgeLevelLoop <= k; connectionEdgeLevelLoop++)
      {
        int portNumero = connectionEdgeLevelLoop; 
        if(connectionEdgeLevelLoop%2)
        {
          //numéro de port impair
          fichier<<"["<<portNumero<<"]\t \"Aggr("<<numPod<<","<<(portNumero-1+k)/2<<",1)\"["<<(numSwitch % (k/2)+1)*2<<"]"<<endl;
        }
        else
        {
          //numéro de port pair, on connecte le noeud de niveau edge à un des hôtes
          fichier<<"["<<portNumero<<"]\t \"Node("<<(edgeNode->getIdDevice()[0] * nbNodesByPod)+ (edgeNode->getIdDevice()[1]*(k/2))+ (portNumero/2) -1<<")\"[1]"<<endl;
        }
      }
      listEdgeNodes.push_back(edgeNode);

    }
    fichier<<"\n\n #Aggregation Level en dessous \n\n";
    for(int aggrLoop = 0; aggrLoop < nbNodesAggregationLevel ; aggrLoop++)
    {
      Device * aggrNode = new Device(Device::levelDev::Aggr);
      aggrNode->setPortNumberDevice(k);
      aggrNode->setIdDevice({aggrLoop/(k/2),aggrLoop%(k/2)+(k/2),1});
      listAggrNodes.push_back(aggrNode);
      int numSwitch = (aggrLoop%(k/2))+k/2;
      int numPod = (aggrLoop/(k/2));
      fichier<<endl<<*aggrNode;
      for(int aggrConnectionLoop = 1 ; aggrConnectionLoop <= k ; aggrConnectionLoop++ )
      {
        int portNumero = aggrConnectionLoop;
        if(aggrConnectionLoop%2)
        {
          //numéro de port impair, on connecte donc ce port à un noeud de niveau core
          fichier<<"["<<portNumero<<"]\t\"Core("<< k<<","<<numSwitch-(k/2)+1<<","<< ceil((double)portNumero/2) <<")\"["<< numPod+1<<"]"<<endl;
        }
        else
        {
          //numéro de port pair, on connecte donc ce port à un noeud de niveau edge
          fichier<<"["<<portNumero<<"]\t\"Edge("<<numPod<<","<<(portNumero/2)-1<<",1)\"["<<(numSwitch-(k/2))*2+1<<"]"<<endl;
        }
      }
    }
    fichier<<"\n\n #Core Level en dessous \n\n";
    for(int coreLoop = 0; coreLoop < nbNodesCoreLevel ; coreLoop++)
    {
      Device * coreNode = new Device(Device::Core);
      coreNode->setIdDevice({k,(coreLoop/(k/2))+1,(coreLoop%(k/2))+1});
      coreNode->setPortNumberDevice(k);
      listCoreNodes.push_back(coreNode);
      int currentGroup = (coreLoop/(k/2))+1;
      int currentCore = (coreLoop%(k/2))+1; 
      fichier<<*coreNode; 
      for(int coreConnectionLoop = 1 ; coreConnectionLoop <= k ; coreConnectionLoop++)
      {
        int portNumero = coreConnectionLoop;
        fichier<<"["<<coreConnectionLoop<<"]\t \"Aggr("<<coreConnectionLoop-1<<","<<(k/2)+currentGroup-1<<",1)\"["<< (currentCore *2)-1 <<"]"<<endl;
      }
      fichier<<endl;
    }
    for(Device* elem : listAggrNodes)
    {
      delete elem;
    }
    for(Device * elem : listEdgeNodes)
    {
      delete elem;
    }    
    for(Device * elem : listLeafNodes)
    {
      delete elem;
    }
        for(Device * elem : listCoreNodes)
    {
      delete elem;
    }

    if(fichier.is_open()) 
    {
      fichier.close();
      }
  }
  return 0;
  }
}
