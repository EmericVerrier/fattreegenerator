#include "Device.hpp"
#include <stdexcept>
#include <ostream>
#include <sstream>
Device::Device(levelDev level)
{
  if(level == levelDev::Leaf)
  {
    portNumberDevice=1;
    typeDevice = typeDev::Hca;
    idDevice.assign(1,0);
  }
  else
  {
    portNumberDevice = 2;
    typeDevice = typeDev::Switch;
    idDevice.assign(3,0);
  }
  levelDevice = level;
  nameDevice ="";
}
std::vector<int> Device::getIdDevice() const
{
  return idDevice;
}
void Device::setIdDevice(std::vector<int> vectorId)
{
  if(vectorId.size() != 3 || (vectorId.size() != 1 && typeDevice == typeDev::Hca))
  {
    throw std::length_error("given idVector has an incorrect length");
  }
  else
  {
    idDevice = vectorId;
    setNameDevice();
  }
}
void Device::setNameDevice()
{
  std::stringstream stringStr;
  stringStr<<"\""+getLevelDevice()<<"(";
  for(std::vector<int>::iterator it = idDevice.begin(); it != idDevice.end(); ++it)
  {
    stringStr<<*it;
    if(it+1 != idDevice.end())
    {
      stringStr<<",";
    }
  }
  stringStr<<")\"";
  nameDevice = stringStr.str();

  
}
void Device::setIdDevice(int idDev)
{
  if(typeDevice == typeDev::Hca)
  {
    idDevice[0] = idDev;
    setNameDevice();
  }
  else
  {
    throw(std::invalid_argument("it works only if your device type is Hca"));
  }
}
void Device::setPortNumberDevice(int portNumber)
{
  portNumberDevice = portNumber;
}
int Device::getPortNumberDevice() const
{
  return portNumberDevice;
}
std::string Device::getTypeDevice() const
{
  switch(typeDevice){
    case(Hca):
      return "Hca";
    break;
    case(Switch):
      return "Switch";
    break;
  }
  return "";
}
std::string Device::getLevelDevice() const
{
  switch(levelDevice)
  {
    case(Leaf):
      return "Node";
    break;
    case(Aggr):
      return "Aggr";
    break;
    case(Core):
      return "Core";
    break;
    case(Edge):
      return "Edge";
    break;
  }
  return "";
}
std::string Device::getNameDevice() const
{
  return nameDevice;
}
std::ostream& operator<<(std::ostream& s,  const Device& device )
{
  s<< device.getTypeDevice()<<" "<<device.getPortNumberDevice()<<" "<< device.getNameDevice() <<std::endl;
  return s;
}
    
    