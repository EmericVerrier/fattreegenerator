#include <string>
#include <vector>
#include <ostream>
class Device
{
public:
  enum typeDev{ Hca, Switch };
  enum levelDev {Core, Aggr, Edge, Leaf};
  Device(levelDev level);
  void setPortNumberDevice(int portNumber);
  
  std::vector<int> getIdDevice() const;
  int getPortNumberDevice() const ;
  std::string getTypeDevice() const;
  std::string getLevelDevice()const;
  std::string getNameDevice() const;
  void setIdDevice(std::vector<int> vectorId);
  void setIdDevice(int idDev);
private :
  void setNameDevice();
  typeDev typeDevice;
  levelDev levelDevice;
  int portNumberDevice;
  std::string nameDevice;
  std::vector<int> idDevice;

};
  std::ostream& operator<<(std::ostream &s ,const Device &device);
